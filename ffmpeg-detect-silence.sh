#!/bin/bash

thresh="-10"
duration=2

[ $# -eq 0 ] && echo -e "Usage: \nArg1: Path to video file\nArg2: Optional -f flag to format output to HH:MM:SS.fff" && exit

[[ -z "$1" || ! -f "$1" ]] && echo "You must provide an existing video file." && exit

output=$( ffmpeg -hide_banner -vn -i "$1" -af "silencedetect=n=${thresh}dB:d=${duration}" -f null - 2>&1 | \
	grep -o '\(silence_start\|silence_end\): [0-9]\+\.\?[0-9]\+\?' | \
	cut -d ' ' -f2 )

[ "$2" == "-f" ] && output=$( echo "$output" | xargs -I {} date -ud @{} +%T.%3N )

echo "$output"
